# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-21 23:47+0000\n"
"PO-Revision-Date: 2023-01-05 14:47+0000\n"
"Last-Translator: Besnik Bleta <besnik@programeshqip.org>\n"
"Language-Team: Albanian <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/sq/>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: forms.py:53
msgid "Add a tag..."
msgstr "Shtoni një etiketë…"

#: forms.py:55
msgid "Add"
msgstr "Shtoje"

#: forms.py:56
msgid "use commas to add multiple tags"
msgstr "që të ndani nga njëra-tjetra etiketa të shumta, përdorni presje"

#: forms.py:64
msgid "Attach a file"
msgstr "Bashkëngjitni një kartelë"

#: forms.py:65
msgid "Attach another file"
msgstr "Bashkëngjitni një tjetër kartelë"

#: forms.py:66
msgid "Remove this file"
msgstr "Hiqe këtë kartelë"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "Gabim 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "Oh, Mos!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "S’e gjej dot këtë faqe."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "Shko mbrapsht te kreu"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "Gabim 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr ""
"Na ndjeni, por faqja e kërkuar s’kapet dot, për shkak të një ngecjeje të "
"shërbyesit."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:23
msgid "started"
msgstr "nisur më"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:23
msgid "last active:"
msgstr "aktiv së fundi më:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "shiheni këtë rrjedhë"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(pa sugjerime)"

#: templates/hyperkitty/ajax/temp_message.html:12
msgid "Sent just now, not yet distributed"
msgstr "Sapo u dërgua, ende i pashpërndarë"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "API REST"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"HyperKitty vjen me një API të vockël REST që ju lejon të merrni email-e dhe "
"të dhëna sipas një programi."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "Formate"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"Kjo API REST mund t’i sjellë të dhënat në disa formate të ndryshme.  Formati "
"parazgjedhje është HTML, për të lejuar lexueshmëri nga syri njerëzor."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"Që të ndryshoni formatin, thjesht shtoni <em>?format=&lt;FORMAT&gt;</em> te "
"URL-ja."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "Lista e formateve të gatshëm është:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "Tekst i thjeshtë"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "Listë listash postimesh"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "Pikëmbarim:"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr ""
"Duke përdorur këtë adresë do të jeni në gjendje të merrni të dhënat e "
"njohura për krejt listat e postimeve."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "Rrjedha në një listë postimesh"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"Duke përdorur këtë adresë do të jeni në gjendje të merrni të dhëna rreth "
"krejt rrjedhave në listën e treguar të postimeve."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "Email-e në një rrjedhë"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"Duke përdorur këtë adresë do të jeni në gjendje të merrni listën e email-eve "
"në një rrjedhë liste postimesh."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "Një email në një listë postimesh"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"Duke përdorur këtë adresë do të jeni në gjendje të merrni të dhëna rreth një "
"email-i të caktuar në listën e treguar të postimeve."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "Etiketa"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr ""
"Duke përdorur këtë adresë do të jeni në gjendje të merrni listën e etiketave."

#: templates/hyperkitty/base.html:56 templates/hyperkitty/base.html:135
msgid "Account"
msgstr "Llogari"

#: templates/hyperkitty/base.html:61 templates/hyperkitty/base.html:140
msgid "Mailman settings"
msgstr "Rregullime Mailman-i"

#: templates/hyperkitty/base.html:66 templates/hyperkitty/base.html:145
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "Veprimtari postimi"

#: templates/hyperkitty/base.html:71 templates/hyperkitty/base.html:150
msgid "Logout"
msgstr "Dalje"

#: templates/hyperkitty/base.html:84
msgid "Manage this list"
msgstr "Administroni këtë listë"

#: templates/hyperkitty/base.html:89
msgid "Manage lists"
msgstr "Administroni lista"

#: templates/hyperkitty/base.html:96
msgid "Sign In"
msgstr "Hyni"

#: templates/hyperkitty/base.html:100
msgid "Sign Up"
msgstr "Regjistrohuni"

#: templates/hyperkitty/base.html:108
msgid "Search this list"
msgstr "Kërkoni te kjo listë"

#: templates/hyperkitty/base.html:111
msgid "Search all lists"
msgstr "Kërkoni te krejt listat"

#: templates/hyperkitty/base.html:196
msgid "Keyboard Shortcuts"
msgstr "Shkurtore Tastiere"

#: templates/hyperkitty/base.html:199
msgid "Thread View"
msgstr "Pamje Rrjedhë"

#: templates/hyperkitty/base.html:201
msgid "Next unread message"
msgstr "Mesazhi pasues i palexuar"

#: templates/hyperkitty/base.html:202
msgid "Previous unread message"
msgstr "Mesazhi i mëparshëm i palexuar"

#: templates/hyperkitty/base.html:203
msgid "Jump to all threads"
msgstr "Kalo te krejt rrjedhat"

#: templates/hyperkitty/base.html:204
msgid "Jump to MailingList overview"
msgstr "Kalo te përmbledhje MailingList"

#: templates/hyperkitty/base.html:219
msgid "Powered by"
msgstr "Bazuar në"

#: templates/hyperkitty/base.html:219
msgid "version"
msgstr "version"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "Ende e pasendërtuar"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "E pasendërtuar"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "Kjo veçori s’është sendërtuar ende, na ndjeni."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "Gabim: listë private"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr ""
"Kjo listë postimesh është private. Që të shihni arkivat, duhet të jeni i "
"pajtuar."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "E pëlqeni (anuloje)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "S’e pëlqeni (anuloje)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "Që të votoni, lypset të jeni i futur llogarinë tuaj."

#: templates/hyperkitty/fragments/month_list.html:7
msgid "Threads by"
msgstr "Rrjedha nga"

#: templates/hyperkitty/fragments/month_list.html:7
msgid " month"
msgstr " muaj"

#: templates/hyperkitty/fragments/overview_threads.html:11
msgid "New messages in this thread"
msgstr "Mesazhe të rinj në këtë rrjedhë"

#: templates/hyperkitty/fragments/overview_threads.html:36
#: templates/hyperkitty/fragments/thread_left_nav.html:19
#: templates/hyperkitty/overview.html:125
msgid "All Threads"
msgstr "Krejt Rrjedhat"

#: templates/hyperkitty/fragments/overview_top_posters.html:15
msgid "See the profile"
msgstr "Shihni profilin"

#: templates/hyperkitty/fragments/overview_top_posters.html:21
msgid "posts"
msgstr "postime"

#: templates/hyperkitty/fragments/overview_top_posters.html:26
msgid "No posters this month (yet)."
msgstr "(Ende) Pa postues këtë muaj."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "Ky mesazh do të dërgohet si:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "Ndryshoni dërgues"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "Lidhni një tjetër adresë"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr ""
"Nëse s’jeni një anëtar i tanishëm i listës, dërgimi i këtij mesazhi do të "
"kryejë pajtimin tuaj në të."

#: templates/hyperkitty/fragments/thread_left_nav.html:12
#: templates/hyperkitty/threads/right_col.html:26
msgid "List overview"
msgstr "Përmbledhje liste"

#: templates/hyperkitty/fragments/thread_left_nav.html:29
#: templates/hyperkitty/overview.html:139 views/message.py:74
#: views/mlist.py:114 views/thread.py:191
msgid "Download"
msgstr "Shkarkim"

#: templates/hyperkitty/fragments/thread_left_nav.html:32
#: templates/hyperkitty/overview.html:142
msgid "Past 30 days"
msgstr "30 ditët e shkuara"

#: templates/hyperkitty/fragments/thread_left_nav.html:33
#: templates/hyperkitty/overview.html:143
msgid "This month"
msgstr "Këtë muaj"

#: templates/hyperkitty/fragments/thread_left_nav.html:36
#: templates/hyperkitty/overview.html:146
msgid "Entire archive"
msgstr "Krejt arkivën"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:18
msgid "Available lists"
msgstr "Lista të gatshme"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "Renditi sipas numrit të pjesëmarrësve së fundi"

#: templates/hyperkitty/index.html:30 templates/hyperkitty/index.html:33
#: templates/hyperkitty/index.html:88
msgid "Most popular"
msgstr "Më populloret"

#: templates/hyperkitty/index.html:40
msgid "Sort by number of recent discussions"
msgstr "Renditi sipas numrin të diskutimeve së fundi"

#: templates/hyperkitty/index.html:44 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:91
msgid "Most active"
msgstr "Më aktivët"

#: templates/hyperkitty/index.html:54
msgid "Sort alphabetically"
msgstr "Renditi alfabetikisht"

#: templates/hyperkitty/index.html:58 templates/hyperkitty/index.html:61
#: templates/hyperkitty/index.html:94
msgid "By name"
msgstr "Sipas emrash"

#: templates/hyperkitty/index.html:68
msgid "Sort by list creation date"
msgstr "Renditi sipas datës së krijimit të listës"

#: templates/hyperkitty/index.html:72 templates/hyperkitty/index.html:75
#: templates/hyperkitty/index.html:97
msgid "Newest"
msgstr "Më të rejave"

#: templates/hyperkitty/index.html:84
msgid "Sort by"
msgstr "Renditi sipas"

#: templates/hyperkitty/index.html:107
msgid "Hide inactive"
msgstr "Fshih joaktivet"

#: templates/hyperkitty/index.html:108
msgid "Hide private"
msgstr "Fshih privatet"

#: templates/hyperkitty/index.html:115
msgid "Find list"
msgstr "Gjeni listë"

#: templates/hyperkitty/index.html:141 templates/hyperkitty/index.html:209
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "e re"

#: templates/hyperkitty/index.html:153 templates/hyperkitty/index.html:220
msgid "private"
msgstr "private"

#: templates/hyperkitty/index.html:155 templates/hyperkitty/index.html:222
msgid "inactive"
msgstr "joaktive"

#: templates/hyperkitty/index.html:161 templates/hyperkitty/index.html:247
#: templates/hyperkitty/overview.html:65 templates/hyperkitty/overview.html:72
#: templates/hyperkitty/overview.html:79 templates/hyperkitty/overview.html:88
#: templates/hyperkitty/overview.html:96 templates/hyperkitty/overview.html:169
#: templates/hyperkitty/overview.html:186 templates/hyperkitty/reattach.html:37
#: templates/hyperkitty/thread.html:85
msgid "Loading..."
msgstr "Po ngarkohet…"

#: templates/hyperkitty/index.html:178 templates/hyperkitty/index.html:255
msgid "No archived list yet."
msgstr "Ende pa listë të arkivuar."

#: templates/hyperkitty/index.html:190
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "Listë"

#: templates/hyperkitty/index.html:191
msgid "Description"
msgstr "Përshkrim"

#: templates/hyperkitty/index.html:192
msgid "Activity in the past 30 days"
msgstr "Veprimtari gjatë 30 ditëve të shkuara"

#: templates/hyperkitty/index.html:236 templates/hyperkitty/overview.html:178
#: templates/hyperkitty/thread_list.html:57
#: templates/hyperkitty/threads/right_col.html:104
#: templates/hyperkitty/threads/summary_thread_large.html:54
msgid "participants"
msgstr "pjesëmarrës"

#: templates/hyperkitty/index.html:241 templates/hyperkitty/overview.html:179
#: templates/hyperkitty/thread_list.html:62
msgid "discussions"
msgstr "diskutime"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "Fshije Listën e Postimeve"

#: templates/hyperkitty/list_delete.html:18
msgid "Delete Mailing List From HyperKitty"
msgstr "Fshije Listën e Postimeve nga HyperKitty"

#: templates/hyperkitty/list_delete.html:24
msgid ""
"will be deleted from HyperKitty along with all the threads and messages. It "
"will not be deleted from Mailman Core. Do you want to continue?"
msgstr ""
"do të fshihet nga HyperKitty me krejt rrjedhat dhe mesazhet. S’do të fshihet "
"nga Mailman Core. Doni të vazhdohet?"

#: templates/hyperkitty/list_delete.html:31
#: templates/hyperkitty/message_delete.html:42
msgid "Delete"
msgstr "Fshije"

#: templates/hyperkitty/list_delete.html:32
#: templates/hyperkitty/message_delete.html:43
#: templates/hyperkitty/message_new.html:51
#: templates/hyperkitty/messages/message.html:148
msgid "or"
msgstr "ose"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:43
#: templates/hyperkitty/message_new.html:51
#: templates/hyperkitty/messages/message.html:148
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "anuloje"

#: templates/hyperkitty/message.html:20
msgid "thread"
msgstr "rrjedhë"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:18
msgid "Delete message(s)"
msgstr "Fshi mesazhin(et)"

#: templates/hyperkitty/message_delete.html:23
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        Do të fshihet %(count)s mesazh(e). Doni të vazhdohet?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:19
msgid "Create a new thread"
msgstr "Krijoni një rrjedhë të re"

#: templates/hyperkitty/message_new.html:20
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "në"

#: templates/hyperkitty/message_new.html:50
#: templates/hyperkitty/messages/message.html:147
msgid "Send"
msgstr "Dërgoje"

#: templates/hyperkitty/messages/message.html:18
#, python-format
msgid "See the profile for %(name)s"
msgstr "Shihni profilin për %(name)s"

#: templates/hyperkitty/messages/message.html:28
msgid "Unread"
msgstr "Të palexuara"

#: templates/hyperkitty/messages/message.html:45
msgid "Sender's time:"
msgstr "Kohë dërguesi:"

#: templates/hyperkitty/messages/message.html:51
msgid "New subject:"
msgstr "Subjekt i ri:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "Bashkëngjitje:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "Shfaqe nën shkronja të fiksuara"

#: templates/hyperkitty/messages/message.html:81
msgid "Permalink for this message"
msgstr "Permalidhje për këtë mesazh"

#: templates/hyperkitty/messages/message.html:92
#: templates/hyperkitty/messages/message.html:98
msgid "Reply"
msgstr "Përgjigjuni"

#: templates/hyperkitty/messages/message.html:95
msgid "Sign in to reply online"
msgstr "Që të përgjigjeni, bëni hyrjen"

#: templates/hyperkitty/messages/message.html:107
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments.count)s bashkëngjitje\n"
"                "
msgstr[1] ""
"\n"
"                %(email.attachments.count)s bashkëngjitje\n"
"                "

#: templates/hyperkitty/messages/message.html:133
msgid "Quote"
msgstr "Përmendje"

#: templates/hyperkitty/messages/message.html:134
msgid "Create new thread"
msgstr "Krijoni një rrjedhë të re"

#: templates/hyperkitty/messages/message.html:137
msgid "Use email software"
msgstr "Përdor software email-i"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "Mbrapsht te rrjedha"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "Mbrapsht te lista"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "Fshije këtë mesazh"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                nga %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:35
msgid "Recent"
msgstr "Së fundi"

#: templates/hyperkitty/overview.html:39
msgid "Most Active"
msgstr "Më Aktivet"

#: templates/hyperkitty/overview.html:43
msgid "Most Popular"
msgstr "Më Populloret"

#: templates/hyperkitty/overview.html:49
#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "Të parapëlqyer"

#: templates/hyperkitty/overview.html:53
msgid "Posted"
msgstr "U postua"

#: templates/hyperkitty/overview.html:63
msgid "Recently active discussions"
msgstr "Diskutime aktive së fundi"

#: templates/hyperkitty/overview.html:70
msgid "Most popular discussions"
msgstr "Diskutime më popullore"

#: templates/hyperkitty/overview.html:77
msgid "Most active discussions"
msgstr "Diskutimet më aktiv e"

#: templates/hyperkitty/overview.html:84
msgid "Discussions You've Flagged"
msgstr "Diskutime Që U Keni Vënë Shenjë"

#: templates/hyperkitty/overview.html:92
msgid "Discussions You've Posted to"
msgstr "Diskutime Ku Keni Postuar"

#: templates/hyperkitty/overview.html:109
msgid "Home"
msgstr "Kreu"

#: templates/hyperkitty/overview.html:112
msgid "Stats"
msgstr "Statistika"

#: templates/hyperkitty/overview.html:115
msgid "Threads"
msgstr "Rrjedha"

#: templates/hyperkitty/overview.html:121
#: templates/hyperkitty/overview.html:129
#: templates/hyperkitty/thread_list.html:35
msgid "You must be logged-in to create a thread."
msgstr "Duhet të jeni i futur, që të krijoni një rrjedhë."

#: templates/hyperkitty/overview.html:121
msgid "New"
msgstr "E re"

#: templates/hyperkitty/overview.html:131
#: templates/hyperkitty/thread_list.html:38
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"d-none d-md-inline\">Nisni një rrjedhë të r</span><span class="
"\"d-md-none\">R</span>e"

#: templates/hyperkitty/overview.html:156
msgid "Delete Archive"
msgstr "Fshije Arkivin"

#: templates/hyperkitty/overview.html:166
msgid "Activity Summary"
msgstr "Përmbledhje Veprimtarie"

#: templates/hyperkitty/overview.html:168
msgid "Post volume over the past <strong>30</strong> days."
msgstr "Vëllim postimesh gjatë <strong>30</strong> ditëve të shkuara."

#: templates/hyperkitty/overview.html:173
msgid "The following statistics are from"
msgstr "Statistikat vijuese janë prej"

#: templates/hyperkitty/overview.html:174
msgid "In"
msgstr "Në"

#: templates/hyperkitty/overview.html:175
msgid "the past <strong>30</strong> days:"
msgstr "<strong>30</strong> ditët të shkuara:"

#: templates/hyperkitty/overview.html:184
msgid "Most active posters"
msgstr "Postuesit më aktivë"

#: templates/hyperkitty/overview.html:193
msgid "Prominent posters"
msgstr "Postues të spikatur"

#: templates/hyperkitty/overview.html:208
msgid "kudos"
msgstr "lavdi"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "Ribashkëngjitni një rrjedhë"

#: templates/hyperkitty/reattach.html:18
msgid "Re-attach a thread to another"
msgstr "Ribashkëngjitni një rrjedhë te një tjetër"

#: templates/hyperkitty/reattach.html:20
msgid "Thread to re-attach:"
msgstr "Rrjedhë për ribashkëngjitje:"

#: templates/hyperkitty/reattach.html:27
msgid "Re-attach it to:"
msgstr "Ribashkëngjite te:"

#: templates/hyperkitty/reattach.html:29
msgid "Search for the parent thread"
msgstr "Kërko për rrjedhën mëmë"

#: templates/hyperkitty/reattach.html:30
msgid "Search"
msgstr "Kërko"

#: templates/hyperkitty/reattach.html:42
msgid "this thread ID:"
msgstr "ID-ja e kësaj rrjedhe:"

#: templates/hyperkitty/reattach.html:48
msgid "Do it"
msgstr "Bëje"

#: templates/hyperkitty/reattach.html:48
msgid "(there's no undoing!), or"
msgstr "(s’ka zhbërje!), ose"

#: templates/hyperkitty/reattach.html:50
msgid "go back to the thread"
msgstr "kthehu mbrapsht te rrjedha"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "Përfundime kërkimi për"

#: templates/hyperkitty/search_results.html:28
msgid "search results"
msgstr "përfundime kërkimi"

#: templates/hyperkitty/search_results.html:30
msgid "Search results"
msgstr "Përfundime kërkimi"

#: templates/hyperkitty/search_results.html:32
msgid "for query"
msgstr "për kërkesën"

#: templates/hyperkitty/search_results.html:42
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "mesazhe"

#: templates/hyperkitty/search_results.html:55
msgid "sort by score"
msgstr "renditi sipas pikësh"

#: templates/hyperkitty/search_results.html:58
msgid "sort by latest first"
msgstr "renditi me të fundit së pari"

#: templates/hyperkitty/search_results.html:61
msgid "sort by earliest first"
msgstr "renditi me më të hershmit së pari"

#: templates/hyperkitty/search_results.html:82
msgid "Sorry no email could be found for this query."
msgstr "Na ndjeni, s’u gjet dot email për këtë kërkesë."

#: templates/hyperkitty/search_results.html:85
msgid "Sorry but your query looks empty."
msgstr "Na ndjeni, por kërkesa juaj duket e zbrazët."

#: templates/hyperkitty/search_results.html:86
msgid "these are not the messages you are looking for"
msgstr "këta s’janë mesazhet që po kërkonit"

#: templates/hyperkitty/thread.html:26
msgid "newer"
msgstr "më i ri"

#: templates/hyperkitty/thread.html:45
msgid "older"
msgstr "më i vjetër"

#: templates/hyperkitty/thread.html:71
msgid "Show replies by thread"
msgstr "Shfaq përgjigje sipas rrjedhash"

#: templates/hyperkitty/thread.html:74
msgid "Show replies by date"
msgstr "Shfaq përgjigje sipas datash"

#: templates/hyperkitty/thread.html:87
msgid "Visit here for a non-javascript version of this page."
msgstr "Vizitoni këtë për një version pa Javascript të kësaj faqeje."

#: templates/hyperkitty/thread_list.html:71
msgid "Sorry no email threads could be found"
msgstr "Na ndjeni, s’u gjetën rrjedha emaile-sh"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "Klikoni që ta përpunoni"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "Që të përpunoni, duhet të keni bërë hyrjen."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "s’ka kategori"

#: templates/hyperkitty/threads/right_col.html:13
msgid "Age (days ago)"
msgstr "Mosha (ditë më parë)"

#: templates/hyperkitty/threads/right_col.html:19
msgid "Last active (days ago)"
msgstr "Aktiv së fundi (ditë më parë)"

#: templates/hyperkitty/threads/right_col.html:47
#, python-format
msgid "%(num_comments)s comments"
msgstr "%(num_comments)s komente"

#: templates/hyperkitty/threads/right_col.html:51
#, python-format
msgid "%(participants_count)s participants"
msgstr "%(participants_count)s pjesëmarrës"

#: templates/hyperkitty/threads/right_col.html:56
#, python-format
msgid "%(unread_count)s unread <span class=\"hidden-sm\">messages</span>"
msgstr "%(unread_count)s <span class=\"hidden-sm\">mesazhe</span> të palexuar"

#: templates/hyperkitty/threads/right_col.html:66
msgid "You must be logged-in to have favorites."
msgstr "Që të keni të parapëlqyer, duhet të keni bërë hyrjen."

#: templates/hyperkitty/threads/right_col.html:67
msgid "Add to favorites"
msgstr "Shtoje te të parapëlqyerit"

#: templates/hyperkitty/threads/right_col.html:69
msgid "Remove from favorites"
msgstr "Hiqe prej të parapëlqyerve"

#: templates/hyperkitty/threads/right_col.html:78
msgid "Reattach this thread"
msgstr "Ribashkëngjit këtë rrjedhë"

#: templates/hyperkitty/threads/right_col.html:82
msgid "Delete this thread"
msgstr "Fshije këtë rrjedhë"

#: templates/hyperkitty/threads/right_col.html:120
msgid "Unreads:"
msgstr "Të palexuar:"

#: templates/hyperkitty/threads/right_col.html:122
msgid "Go to:"
msgstr "Shko te:"

#: templates/hyperkitty/threads/right_col.html:122
msgid "next"
msgstr "pasuesi"

#: templates/hyperkitty/threads/right_col.html:123
msgid "prev"
msgstr "i mëparshmi"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr ""

#: templates/hyperkitty/threads/summary_thread_large.html:38
msgid "Most recent thread activity"
msgstr "Veprimtaria më e freskët në rrjedhë"

#: templates/hyperkitty/threads/summary_thread_large.html:59
msgid "comments"
msgstr "komente"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "etiketa"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "Kërko për etiketë"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "Hiqe"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "Mesazhe nga"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "Mbrapsht te profili i %(fullname)s"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "Na ndjeni, s’u gjet dot email për këtë përdorues."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "Veprimtari postimi e përdoruesit"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "për"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "Rrjedha që keni lexuar"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "Vota"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "Pajtime"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "Autori fillestar:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "Nisur më:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "Veprimtaria e fundit:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "Përgjigje:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "Subjekt"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "Autori fillestar"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "Datë fillimi"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "Veprimtaria e fundit"

#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "Përgjigje"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "Ende pa të parapëlqyer."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "Komente të reja"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "Ende pa lexuar gjë."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "Postimet e Fundit"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "Datë"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "Rrjedhë"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "Veprimtaria e fundit në rrjedhë"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "Ende s’ka postime."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "që prej postimit të parë"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "postim"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "ende pa postim"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "Kohë që nga veprimtaria e parë"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "Postimi i parë"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "Postime në këtë listë"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "pa pajtime"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "E pëlqeni"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "S’e pëlqeni"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "Votë"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "Ende pa votë."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "Profil Përdoruesi"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "Profil përdoruesi"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "Emër:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "Krijim:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "Vota për këtë përdorues:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "Adresa email:"

#: views/message.py:75
msgid "This message in gzipped mbox format"
msgstr "Këtë mesazh në format mbox gzipp"

#: views/message.py:201
msgid "Your reply has been sent and is being processed."
msgstr "Përgjigja juaj është dërguar dhe po përpunohet."

#: views/message.py:205
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  Jeni pajtuar te lista {}."

#: views/message.py:288
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "S’u fshi dot mesazhi %(msg_id_hash)s: %(error)s"

#: views/message.py:297
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "U fshinë me sukses %(count)s mesazhe."

#: views/mlist.py:88
msgid "for this MailingList"
msgstr "për këtë Listë Postimesh"

#: views/mlist.py:100
msgid "for this month"
msgstr "për këtë muaj"

#: views/mlist.py:103
msgid "for this day"
msgstr "për këtë ditë"

#: views/mlist.py:115
msgid "This month in gzipped mbox format"
msgstr "Këtë muaj në format mbox gzipp"

#: views/mlist.py:250 views/mlist.py:274
msgid "No discussions this month (yet)."
msgstr "(Ende) Pa diskutime këtë muaj."

#: views/mlist.py:262
msgid "No vote has been cast this month (yet)."
msgstr "(Ende) S’ka vota për këtë muaj."

#: views/mlist.py:291
msgid "You have not flagged any discussions (yet)."
msgstr "S’i keni vënë shenjë ndonjë diskutimi (ende)."

#: views/mlist.py:314
msgid "You have not posted to this list (yet)."
msgstr "S’keni postuar te kjo listë (ende)."

#: views/mlist.py:407
msgid "You must be a staff member to delete a MailingList"
msgstr "Duhet të jeni pjesëtar i stafit që të fshini një Listë Postimesh"

#: views/mlist.py:421
msgid "Successfully deleted {}"
msgstr "U fshinë me sukses {}"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "Gabim në përtypje: %(error)s"

#: views/thread.py:192
msgid "This thread in gzipped mbox format"
msgstr "Këtë rrjedhë në format mbox gzipp"

#~ msgid ""
#~ "<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
#~ "\">S</span>ubscription"
#~ msgstr ""
#~ "<span class=\"d-none d-md-inline\">Administroni p</span><span class=\"d-"
#~ "md-none\">P</span>ajtimtarë"

#~ msgid "First Post"
#~ msgstr "Postimi i Parë"

#~ msgid "days inactive"
#~ msgstr "ditë joaktiv"

#~ msgid "days old"
#~ msgstr "ditë i vjetër"

#~ msgid ""
#~ "\n"
#~ "                    by %(name)s\n"
#~ "                    "
#~ msgstr ""
#~ "\n"
#~ "                    nga %(name)s\n"
#~ "                    "

#~ msgid "unread"
#~ msgstr "të palexuara"

#~ msgid "Go to"
#~ msgstr "Shko tek"

#~ msgid "More..."
#~ msgstr "Më tepër…"

#~ msgid "Discussions"
#~ msgstr "Diskutime"

#~ msgid "most recent"
#~ msgstr "më të rejat"

#~ msgid "most popular"
#~ msgstr "më populloret"

#~ msgid "most active"
#~ msgstr "më aktivet"

#~ msgid "Update"
#~ msgstr "Përditësoje"

#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        nga %(name)s\n"
#~ "                                    "
